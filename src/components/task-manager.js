const TaskManager = (function() {
	const tasks = [];
	let filtered = [];
	const filter = "all";
	const filters = document.querySelectorAll("[data-filter]");
	let instance;

	for (let filterItem of filters) {
		filterItem.addEventListener("click", filterBy, true);
	}

	function createTask(title, parent = 0) {
		const id = tasks.length + 1;
		const newTask = {
			id,
			title,
			parent,
			status: 0
		};

		tasks.push(newTask);
		console.log("New task created");
		console.log(tasks);
	}

	function getTotalTasks() {
		return tasks.length;
	}

	function filterBy(e) {
		const name = e.target.dataset.filter;
		const previousFilter = document.querySelector(".filter span.current");
		const nextFilter = e.target;

		previousFilter.classList.remove("current");
		nextFilter.classList.add("current");
	}

	function init() {
		return {
			createTask,
			getTotalTasks,
			filterBy
		};
	}

	return instance || (instance = init());
})();

export default TaskManager;
